# KickForce Martial Arts iOS App

App Demo: https://youtu.be/iyCMy9OWF1I



This app was created for KickForce Martial Arts in San Diego.

The KickForce app will serve as a resource for students to complement their training done in the gym. The app stores videos of basic techniques, forms, weapons trainings, self-defense techniques, acrobatic martial arts moves, and other workout routines. The students will have access to training material from their iOS device to be able train on their own and reflect on their progress, with content tailored to their belt rank and training goals.
Additionally, the app will also foster a sense of community by providing each student with a personal profile as well as a newsfeed/wall section where they can share content and communicate with other KickForce members