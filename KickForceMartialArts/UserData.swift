//
//  UserData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class UserData{
    
    private var _name: String
    private var _firstName: String
    private var _lastName: String
    private var _username: String
    private var _uid: String
    private var _email: String!
    private var _beltColor: String!
    private var _profilePicURL: String!
    private var _profilePic: UIImage?

    var name: String{
        return _name
    }
    var firstName: String{
        return _firstName
    }
    var lastName: String{
        return _lastName
    }
    var username: String{
        return _username
    }
    var uid: String{
        return _uid
    }
    var email: String{
        return _email
    }
    var beltColor: String{
        return _beltColor
    }
    var profilePicURL: String{
        return _profilePicURL
    }
    var profilePic: UIImage{
        return GlobalActions.singleton.getImage(from: self._profilePicURL)
    }
    
    init(firstName: String, lastName: String, username: String, uid: String, email: String, beltColor: String, profilePicURL: String){
        self._name = firstName + " " + lastName
        self._firstName = firstName
        self._lastName = lastName
        self._username = username
        self._uid = uid
        self._email = email
        self._beltColor = beltColor
        self._profilePicURL = profilePicURL
    }
}
