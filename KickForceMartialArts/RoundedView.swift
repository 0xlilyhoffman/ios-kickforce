//
//  RoundedView.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/6/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class RoundedView: UIView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).cgColor
        layer.borderWidth = 0.25
        layer.masksToBounds = true
        layer.cornerRadius = 15.0
        self.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.25)
    }
    
}
