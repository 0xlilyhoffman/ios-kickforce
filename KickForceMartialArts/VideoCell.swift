//
//  VideoCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import SearchTextField

class VideoCell: UICollectionViewCell{
    @IBOutlet var videoTitleLabel: UILabel!
    @IBOutlet var videoImage: UIImageView!
    
    
    func configureCell(videoData: VideoData){
        //Pull image from internet
        let videoThumbnail = GlobalActions.singleton.generateThumbnail(url: URL(string: videoData.videoURL)! as NSURL, fromTime: 0)
        
        //Set data
        videoTitleLabel.text = videoData.videoTitle
        videoImage.image = videoThumbnail

    }
 
    func configureCell(videoData: VideoData, needImage: Bool, cache: NSCache<NSString, UIImage>){
        if needImage{
            //Pull image from internet
            let videoThumbnail = GlobalActions.singleton.generateThumbnail(url: URL(string: videoData.videoURL)! as NSURL, fromTime: 0)
            
            //Set data
            videoTitleLabel.text = videoData.videoTitle
            videoImage.image = videoThumbnail
            
            //Put image in cache
            cache.setObject(videoThumbnail, forKey: videoData.videoURL as NSString)
            
        }else{
            videoTitleLabel.text = videoData.videoTitle
        }
    }
}

