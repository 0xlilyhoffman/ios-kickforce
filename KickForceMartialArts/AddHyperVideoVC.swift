//
//  AddHyperVideoVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

//BAD


import Foundation
import UIKit

class AddHyperVideoVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
   
    //MARK: Properties
    let videoPicker = UIImagePickerController()
    var downloadURL: String! = "error"
    var level: String! = "One"
    var category: String! = "Kicks"
    var presentingVC: UIViewController!

    
    //MARK: IBOutlet
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var rootView: UIView!
    @IBOutlet var videoImage: UIImageView!
    @IBOutlet var videoTitleTextField: UITextField!

    //MARK: IBActions
    @IBAction func videoButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            videoPicker.sourceType = .photoLibrary
            videoPicker.allowsEditing = true
            if (UIImagePickerController.availableMediaTypes(for: .photoLibrary)!).contains("public.movie"){
                videoPicker.mediaTypes = ["public.movie"]
            }
            self.present(videoPicker, animated: true, completion: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Photo Library Access Error", message: "Cannot access photo library")
        }
    }
    
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.removeBlur()
        })
    }
    @IBAction func submitButtonPressed(_ sender: Any) {
        if videoTitleTextField.text == nil || videoTitleTextField.text == ""{
            videoTitleTextField.text = " "
        }
        DataService.singleton.submitHyperVideo(level: level, category: category, title: videoTitleTextField.text!, url: downloadURL)
        dismiss(animated: true, completion: {
            self.removeBlur()
        })
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true

        videoPicker.delegate = self
        
        submitButton.isEnabled = false
        
        addTapToDismissKeyboard()
    }
    
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    
    //MARK: ImagePicker
    //Upload image/video in Firebase storage and get url to put in database upon "submit"
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.allowsEditing = true
        if let pickedVideo = info[UIImagePickerControllerMediaURL] as? URL{
            DataService.singleton.uploadVideoToStorage(category: "Hyper", data: pickedVideo, completed: { (downloadURL) in
                if downloadURL == "error"{
                    GlobalActions.singleton.displayAlert(sender: self, title: "Error on video upload", message: "We could not upload your video at this time. Please try again later")
                }else{
                    self.videoImage.image = GlobalActions.singleton.generateThumbnail(url: NSURL(string: downloadURL)!, fromTime: 0)
                    self.downloadURL = downloadURL
                    self.submitButton.isEnabled = true
                }
            })
        }
        dismiss(animated: true, completion: {
            self.blurView()
        })
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }

    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        removeBlur()
    }

    func removeBlur(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
    
    func blurView(){
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = presentingVC.view.frame
        presentingVC.view.addSubview(blurEffectView)
    }
}
