//
//  NewsfeedPostVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase


class NewsfeedPostVC: UIViewController, UITextViewDelegate, PopUpSenderDelegate{
   
    //MARK: Properties
    var firebaseUser: User!
    var user: UserData!
    var selectedUsers = [PopUpSelectionData]()
    var presentingVC: UIViewController!
    
    //MARK: IBOutlets
    @IBOutlet var rootView: UIView!
    @IBOutlet var newPostText: UITextView!
    
    //MARK: IBActions
    @IBAction func tagSomeoneButtonTapped(_ sender: Any) {
        performSegue(withIdentifier: "PresentUsersPopUp", sender: nil)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        DataService.singleton.deleteOldestPost()
        
        if newPostText.text == nil || newPostText.text == ""{
            newPostText.text = " "
        }
        
        var selectedUserString = String()
        for selectedUser in selectedUsers{
            selectedUserString.append("\n@\(selectedUser.title)")
        }
        
        newPostText.text = newPostText.text + "\n" + selectedUserString
        
        for selectedUser in selectedUsers{
            DataService.singleton.submitNotification(receiverUID: selectedUser.uidRef, text: newPostText.text, senderName: user.firstName + " " + user.lastName, timestamp: Date().timeIntervalSince1970)
        }

        DataService.singleton.submitNewsfeedPost(postText: newPostText.text, postAuthor: user.name, postAuthorUID: user.uid, postAuthorPic: user.profilePicURL, postTimestamp: Date().timeIntervalSince1970)
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true

        newPostText.delegate = self
        newPostText.textColor = UIColor.darkGray
        newPostText.text = "New Post"
        
        
        firebaseUser = Auth.auth().currentUser
        DataService.singleton.getUserData(userUID: firebaseUser.uid, completed: { (user) in
            self.user = user
        })
        
        addTapToDismissKeyboard()
    }
    
    //MARK: Dismiss keyboard
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    //MARK: PopUpSenderDelegate
    func dataTransfer(users: [PopUpSelectionData]) {
        self.selectedUsers = users
    }
    
    //MARK: TextViewDelegate
    func textViewDidBeginEditing(_ textView: UITextView){
        if (textView.text == "New Post"){
            textView.text = ""
            textView.textColor = .black
        }
        textView.becomeFirstResponder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView){
        if (textView.text == ""){
            textView.text = "New Post"
            textView.textColor = UIColor.darkGray
        }
        textView.resignFirstResponder()
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PresentUsersPopUp" {
            let destVC = segue.destination as! TagUsersPopup
            destVC.delegate = self
        }
    }
    
    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        removeView()
    }
    
    //removes blur view from presenting vc
    func removeView(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }

    
}
