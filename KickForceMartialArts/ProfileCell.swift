//
//  ProfileCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/6/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class ProfileCell: UICollectionViewCell{
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).cgColor
        layer.borderWidth = 0.25
        layer.shadowColor = UIColor.darkGray.cgColor
        layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
        layer.shadowRadius = 5.0
        layer.shadowOpacity = 1.0
        layer.cornerRadius = 15.0
    }
    
    @IBOutlet var postImage: UIImageView!
    

    func configureCell(photoData: ProfilePhotoData, cache: NSCache<NSString, ProfilePhotoData>){
        //Pull image from firebase
        if (photoData.type == "image"){
            photoData.image = GlobalActions.singleton.getImage(from: photoData.mediaURL)
        }else if(photoData.type == "video"){
            photoData.image = GlobalActions.singleton.generateThumbnail(url: URL(string: photoData.mediaURL)! as NSURL ,fromTime: 0)
        }else{
            photoData.image = #imageLiteral(resourceName: "default_user")
        }

        //Set photo image
        postImage.image = photoData.image
        
        //Put COMPLETE object in cache
        cache.setObject(photoData, forKey: photoData.mediaURL as NSString)
    }
    
    
    
    
}
