//
//  ProfilePostVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class ProfilePostVC: UIViewController{
    
    //MARK: Properties
    var presentingVC: ProfileVC!
    var photo: ProfilePhotoData!
    var userUID: String!
    
    //IBOutlet
    @IBOutlet var rootView: UIView!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var captionLabel: UILabel!

    //IBActions
    @IBAction func deleteButtonPressed(_ sender: Any) {
        DataService.singleton.deleteProfilePhoto(userUID: userUID, photoRef: photo.mediaReference)
        dismiss(animated: true, completion: {
            self.refreshPhotos()
        })
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postImage.image = photo.image
        captionLabel.text = photo.caption
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
    }
    
    //Reloads photos (called after deleting a photo) to reflect changes
    func refreshPhotos(){
        DataService.singleton.getProfilePhotos(userUID: userUID, completed: { (photos) in
            self.presentingVC.photos = photos
            self.presentingVC.collectionView.reloadData()
        })
    }
    
    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        removeView()
    }
    
    //Removes blur effect from presenting view 
    func removeView(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
}
