//
//  ProfilePhotoData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class ProfilePhotoData{
    private var _mediaReference: String!
    private var _mediaURL: String!
    private var _caption: String!
    private var _type: String!
    private var _image: UIImage?
    private var _timestamp: Double!
    
    var mediaReference: String{
        return _mediaReference
    }
    var mediaURL: String{
        return _mediaURL
    }
    var type: String{
        return _type
    }
    var caption: String{
        return _caption
    }
    var image: UIImage{
        get{
            if _image == nil{
                return #imageLiteral(resourceName: "default_user")
            }else{
                return _image!
            }
        }set{
            self._image = newValue
        }
    }
    var timestamp: Double{
        return _timestamp
    }
    

    
    init(mediaReference: String, mediaURL: String, type: String, caption: String, timestamp: Double){
        self._mediaReference = mediaReference
        self._mediaURL = mediaURL
        self._type = type
        self._timestamp = timestamp
        self._caption = caption
    }
    
    
}
