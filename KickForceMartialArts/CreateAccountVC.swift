//
//  CreateAccountVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class CreateAccountVC: UIViewController{
    var newUser: UserData!
    
    //MARK: IBOutlets
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var kickForceCodeTextField: UITextField!

    //MARK: IBActions
    @IBAction func createAccountButtonPressed(_ sender: Any) {
        
        //Check that all fields have been filled in. If not, present error and return.
        if (emailTextField.text == nil || emailTextField.text == "" ||
            usernameTextField.text == nil || usernameTextField.text == "" ||
            firstNameTextField.text == nil || firstNameTextField.text == "" ||
            lastNameTextField.text == nil || lastNameTextField.text == "" ||
            passwordTextField.text == nil || passwordTextField.text == "" ||
            kickForceCodeTextField.text == nil || kickForceCodeTextField.text == ""){
            GlobalActions.singleton.displayAlert(sender: self, title: "Insufficient Information", message: "Please fill in all fields")
            return
        }
        
        //Check that KickForce code is correct. If not, present error and return.
        DataService.singleton.getKickForceCode { (code) in
            if (self.kickForceCodeTextField.text! != code){
                GlobalActions.singleton.displayAlert(sender: self, title: "KickForceCode incorrect", message: "Please contact KickForce for help")
                return
            }
        }
        
        //Here -> all fields filled in and kickforce code correct. 
        //Create user in Firebase Authentication and Firebase Database.
        //Return to login
        AuthService.singleton.createAccount(sender: self, email: emailTextField.text!, password: passwordTextField.text!) { (success, message) in
            if success{
                let firebaseUser: User = Auth.auth().currentUser!
                
                self.newUser = UserData(firstName: self.firstNameTextField.text!, lastName: self.lastNameTextField.text!, username: self.usernameTextField.text!, uid: firebaseUser.uid, email: self.emailTextField.text!, beltColor: "White", profilePicURL: "https://firebasestorage.googleapis.com/v0/b/kickforce-a1f7c.appspot.com/o/defaultUser.png?alt=media&token=d1ef330f-2798-442c-9930-17bc0d7b1585")
                
                DataService.singleton.submitNewUser(user: self.newUser){
                    self.performSegue(withIdentifier: "UserCreated", sender: nil)
                }
            }
        }
    }
  
    //MARK: Init
    //Add tap to dismiss keyboard functionality
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapToDismissKeyboard()
    }
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
