//
//  NotificationData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

/*
 Users will recieve notifications when they are tagged in a kickforce wall post
 */


import Foundation
import UIKit

class NotificationData{
    
    //Private vars
    private var _notificationText: String!
    private var _senderName: String!
    private var _timestamp: Double!
    private var _dateString: String
    
    //Public vars
    var notificationText: String{
        return _notificationText
    }
    var senderName: String{
        return _senderName
    }
    
    var timestamp: Double{
        return _timestamp
    }
    
    var dateString: String{
        return _dateString
    }
    
    init(notificationText: String, senderName: String, timestamp: Double){
        self._notificationText = notificationText
        self._senderName = senderName
        self._timestamp = timestamp
        self._dateString = GlobalActions.singleton.getDateString(from: timestamp)
    }
    

    
    
    
    
    
}
