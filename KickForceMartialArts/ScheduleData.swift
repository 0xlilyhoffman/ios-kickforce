//
//  ScheduleData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation

class ScheduleData{
    private var _day: String!
    private var _title: String!
    private var _time: String!
    private var _selected: Bool!
    
    
    init(day: String, title: String, time: String){
        self._day = day
        self._title = title
        self._time = time
        self._selected = false
    }
    
    var day: String{
        return _day
    }
    
    var title: String{
        return _title
    }
    
    var time: String{
        return _time
    }
    
    var selected: Bool{
        get{
            return _selected
        }set{
            _selected = newValue
        }
    }
    
}
