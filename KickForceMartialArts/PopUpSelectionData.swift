//
//  PopUpSelectionData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/9/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit


class PopUpSelectionData{
    private var _title: String!
    private var _uidRef: String!
    private var _selected: Bool!
    
    var uidRef: String{
        return _uidRef
    }
    var title: String{
        return _title
    }
    var selected: Bool{
        get{
            return _selected
        }set{
            _selected = newValue
        }
    }
    
    init(uidRef: String, title: String, selected: Bool){
        self._title = title
        self._selected = selected
        self._uidRef = uidRef
    }
    
}
