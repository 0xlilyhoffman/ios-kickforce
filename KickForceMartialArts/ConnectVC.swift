//
//  ConnectVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import MessageUI


class ConnectVC: UIViewController, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate{
    
    //MARK: IBActions
    //Open weblinks corresponding to each button
    @IBAction func kickforceWebsiteButtonTapped(_ sender: Any) {
        let url = URL(string: "http://www.kickforceata.com")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Hyperlink Error", message: "Could not open KickForce website")
        }
    }
    
    @IBAction func facebookButtonTapped(_ sender: Any) {
        let url = URL(string: "http://www.facebook.com/kickforcema")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Hyperlink Error", message: "Could not open KickForce facebook")
        }
    }
    @IBAction func youtubeButtonTapped(_ sender: Any) {
        let url = URL(string: "https://www.youtube.com/channel/UCo5knKkd_bP9HFB0b4Q2wtg")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Hyperlink Error", message: "Could not open KickForce youtube")

        }
    }
    @IBAction func instagramButtonTapped(_ sender: Any) {
        let url = URL(string: "https://www.instagram.com/kickforcema/")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Hyperlink Error", message: "Could not open KickForce instagram")

        }
    }
    @IBAction func snapchatButtonTapped(_ sender: Any) {
        let url = URL(string: "https://www.snapchat.com/add/kickforce11")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Hyperlink Error", message: "Could not open KickForce snapchat")

        }
    }
    @IBAction func yelpButtonTapped(_ sender: Any) {
        let url = URL(string: "http://www.yelp.com/biz/kickforce-martial-arts-san-diego-2")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Hyperlink Error", message: "Could not open KickForce yelp page")
        }
    }
    
    //Opens text message to 81010 of "join kickforce"
    @IBAction func textJoinKickForceButtonTapped(_ sender: Any) {
        // Make sure the device can send text messages
        if (MFMessageComposeViewController.canSendText()) {
            
            // Obtain a configured MFMessageComposeViewController
            let messageComposeVC = MFMessageComposeViewController()
            messageComposeVC.messageComposeDelegate = self
            messageComposeVC.recipients = ["81010"]
            messageComposeVC.body = "join kickforce"
            
            // Present the configured MFMessageComposeViewController instance
            present(messageComposeVC, animated: true, completion: nil)
        } else {
            GlobalActions.singleton.displayAlert(sender: self, title: "Error Sending Text", message: "Your devices is not able to send text messages")
        }

    }
    
    //Opens open to call kickforce
    @IBAction func callButtonTapped(_ sender: Any) {
        let phoneNumber = "8585785425"
        if let phoneCallURL = URL(string: "tel://\(phoneNumber)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                application.open(phoneCallURL, options: [:], completionHandler: nil)
            }
        }
    }

    //Opens blank email to kickforce
    @IBAction func emailButtonTapped(_ sender: Any) {
        
        //Obtain a configured MFMailComposeViewController
        let mailComposeViewController = MFMailComposeViewController()
        mailComposeViewController.mailComposeDelegate = self
        mailComposeViewController.setToRecipients(["kickforcemartialarts@gmail.com"])
        
        //Check that device can send mail and present email view
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            GlobalActions.singleton.displayAlert(sender: self, title: "Error sending email", message: "Your device could not send e-mail.  Please check your device's e-mail configuration and try again")
        }
    }
    
    
    //MARK: MFMessageComposeViewControllerDelegate
    //ViewController to support "for text message updates, text "join kickforce" to 81010"
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    

    
    
    
    
    
    
    
    
    
    
}
