//
//  VideoPlayerVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/9/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import AVKit
import AVFoundation

class VideoPlayerVC: AVPlayerViewController{
    var videoURL: NSURL!
    
    
    override func viewDidLoad() {
        let player = AVPlayer(url: videoURL as URL)
        self.player = player
        self.showsPlaybackControls = true
        self.navigationController?.navigationBar.isTranslucent = false
        player.play()
    }
    
}
