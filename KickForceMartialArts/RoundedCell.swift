//
//  RoundedCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hofman on 8/6/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class RoundedCell: UICollectionViewCell{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.borderColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0).cgColor
        layer.borderWidth = 0.25
        
        
         layer.shadowColor = UIColor.darkGray.cgColor
         layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
         layer.shadowRadius = 5.0
         layer.shadowOpacity = 1.0
 
        
        layer.cornerRadius = 15.0
    }
}
