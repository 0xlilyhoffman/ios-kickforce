//
//  NewsfeedVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class NewsfeedVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    //MARK: Properties
    var newsfeedPosts = [NewsfeedData]()
    var firebaseUser: User!
    static var newsfeedCache: NSCache<NSString, UIImage> = NSCache()

    //IBOutlet
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    @IBOutlet var tableView: UITableView!
    @IBAction func newPostButtonPressed(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        
        performSegue(withIdentifier: "NewFeedPost", sender: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseUser = Auth.auth().currentUser
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.addSubview(self.refreshControl)

        DataService.singleton.getNewsfeedPosts(userUID: firebaseUser.uid, completed: { (newsfeed) in
            self.newsfeedPosts = newsfeed
            self.tableView.reloadData()
            self.activitySpinner.stopAnimating()
        })
    }
    
    //MARK: RefreshControl
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(self.handleRefresh), for: UIControlEvents.valueChanged)
        return refreshControl
    }()
    
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        DataService.singleton.getNewsfeedPosts(userUID: firebaseUser.uid, completed: { (newsfeed) in
            self.newsfeedPosts = newsfeed
            self.tableView.reloadData()
            refreshControl.endRefreshing()
        })
    }
    
    //MARK: UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsfeedCell", for: indexPath) as! NewsfeedCell
        
        //Check cache for user profile image image
        if let photo = NewsfeedVC.newsfeedCache.object(forKey: newsfeedPosts[indexPath.row].postAuthorPicURL as NSString){
            cell.configureCell(post: newsfeedPosts[indexPath.row], needPhoto: false)
            cell.authorImage.image = photo
        }else{
            cell.configureCell(post: newsfeedPosts[indexPath.row], needPhoto: true)
        }
        
        //Tapping user's name opens that user's profile
        cell.tapAction = { (cell) in
            DataService.singleton.getUserData(userUID: self.newsfeedPosts[indexPath.row].postAuthorUID, completed: { (user) in
                self.performSegue(withIdentifier: "ViewUserProfile", sender: user)
            })
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsfeedPosts.count
    }
    
    //Allow users to delete their own posts
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            if(newsfeedPosts[indexPath.row].postAuthorUID == firebaseUser.uid){
                DataService.singleton.deleteNewsfeedPost(postRef: newsfeedPosts[indexPath.row].postReference)
                self.newsfeedPosts.remove(at: indexPath.row)
                self.tableView.reloadData()
            }
        }
    }


    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewFeedPost"{
            let destVC = segue.destination as! NewsfeedPostVC
            destVC.presentingVC = self
        }
        if segue.identifier == "ViewUserProfile"{
            let destVC = segue.destination as! UserProfileVC
            destVC.user = sender as! UserData
        }
    }

    
}
