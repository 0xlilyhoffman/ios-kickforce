//
//  StoreCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class StoreCell: UICollectionViewCell{
    
    var tapAction: ((UICollectionViewCell) -> Void)?

    @IBOutlet var itemImage: UIImageView!
    @IBOutlet var itemTitleLabel: UILabel!
    @IBOutlet var itemPriceLabel: UILabel!
    @IBAction func requestPurchaseButtonPressed(_ sender: Any) {
        tapAction?(self)
    }
    
    func configureCell(storeItem: StoreData){
        itemTitleLabel.text = storeItem.title
        itemPriceLabel.text = storeItem.price
        itemImage.image = storeItem.image
    }
    
    func configureCell(storeItem: StoreData, needImage: Bool){
        if needImage{
            //Get image from internet
            itemImage.image = storeItem.image
            
            //Set data
            itemTitleLabel.text = storeItem.title
            itemPriceLabel.text = storeItem.price
            
            //Put image in cache
            StoreVC.imageCache.setObject(itemImage.image!, forKey: storeItem.imageURL as NSString)
        }else{
            //Set data
            itemTitleLabel.text = storeItem.title
            itemPriceLabel.text = storeItem.price
        }
    }
}
