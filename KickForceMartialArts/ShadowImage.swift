//
//  ShadowImage.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/6/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class ShadowImage: UIImageView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
         layer.shadowColor = UIColor.darkGray.cgColor
         layer.shadowOffset = CGSize(width: 5.0, height: 5.0)
         layer.shadowRadius = 20.0
         layer.shadowOpacity = 1.0
    }
}
