//
//  DeleteStoreItemVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class DeleteStoreItemVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties 
    var storeItems = [StoreData]()

    //MARK: IBOutlets
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    
    //MARK: IBActions
    @IBAction func addButtonPressed(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        
        performSegue(withIdentifier: "AddStoreItem", sender: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()
        
        collectionView.delegate = self
        collectionView.dataSource = self
    
        DataService.singleton.getStorePosts(completed: { (store) in
            self.storeItems = store
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
    }
    
    //MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell", for: indexPath) as! StoreCell
        cell.configureCell(storeItem: storeItems[indexPath.row])
        
        cell.tapAction = { (cell) in
            DataService.singleton.deleteStoreItem(itemUID: self.storeItems[indexPath.row].uid)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 20) / 2
        return CGSize(width: side, height: side + 90)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        DataService.singleton.deleteStoreItem(itemUID: storeItems[indexPath.row].uid)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddStoreItem"{
            let destVC = segue.destination as! AddStoreItemVC
            destVC.presentingVC = self
        }
    }
}
