//
//  NewProfilePostVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class NewProfilePostVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: Properties
    var presentingVC: ProfileVC!
    var postAuthor: String!
    var postAuthorUID: String!
    var postAuthorPicURL: String!
    var firebaseUser: User!
    var downloadURL: String!
    var type: String!
    let imagePicker = UIImagePickerController()
    let videoPicker = UIImagePickerController()
    
    //MARK: IBOutlets
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var rootView: UIView!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var caption: UITextField!

    //MARK: IBActions
    @IBAction func videoButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            videoPicker.sourceType = .photoLibrary
            videoPicker.allowsEditing = true
            if (UIImagePickerController.availableMediaTypes(for: .photoLibrary)!).contains("public.movie"){
                videoPicker.mediaTypes = ["public.movie"]
            }
            self.present(videoPicker, animated: true, completion: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Photo Library Access Error", message: "Cannot access photo library")
        }
        
    }
    @IBAction func photoButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            if (UIImagePickerController.availableMediaTypes(for: .photoLibrary)!).contains("public.image"){
                imagePicker.mediaTypes = ["public.image"]
            }
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Photo Library Access Error", message: "Cannot access photo library")
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.removeBlur()
        })
    }
    @IBAction func submitButtonPressed(_ sender: Any) {
        if caption.text == nil || caption.text == ""{
            caption.text = " "
        }
        
        DataService.singleton.submitProfilePostToDatabase(userUID: self.firebaseUser.uid, downloadURL: self.downloadURL, type: self.type, caption: self.caption.text!)
        dismiss(animated: true, completion: {
            self.removeBlur()
            self.refreshPhotos()
        })
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submitButton.isEnabled = false
        
        imagePicker.delegate = self
        videoPicker.delegate = self
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
        
        firebaseUser = Auth.auth().currentUser!
        addTapToDismissKeyboard()
    }
    
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func refreshPhotos(){
        DataService.singleton.getProfilePhotos(userUID: firebaseUser.uid, completed: { (photos) in
            self.presentingVC.photos = photos
            self.presentingVC.collectionView.reloadData()
        })
    }
    
    //Adds blur view from presenting view
    func blurView(){
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = presentingVC.view.frame
        presentingVC.view.addSubview(blurEffectView)
    }
    
    //Removes blur view from presenting view
    func removeBlur(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }
    
    //MARK: ImagePicker
    //Upload image/video in Firebase storage and get url to put in database upon "submit"
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if (picker == imagePicker){
            picker.allowsEditing = true
            if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
                postImage.image = pickedImage
                let data = UIImageJPEGRepresentation(pickedImage, 1.0)! as NSData
                
                DataService.singleton.uploadProfilePhotoPostToStorage(userUID: firebaseUser.uid, data: data, completed: { (downloadURL) in
                    if downloadURL == "error"{
                        GlobalActions.singleton.displayAlert(sender: self, title: "Error on photo upload", message: "We could not upload your photo at this time. Please try again later")
                    }else{
                        self.downloadURL = downloadURL
                        self.postImage.image = pickedImage
                        self.type = "image"
                        self.submitButton.isEnabled = true
                    }
                    self.blurView()
                    self.dismiss(animated: true, completion: {
                        self.blurView()
                        self.postImage.image = pickedImage
                    })
                })
            }
        }
        if(picker == videoPicker){
            picker.allowsEditing = true
            if let pickedVideo = info[UIImagePickerControllerMediaURL] as? URL{
                DataService.singleton.uploadProfileVideoPostToStorage(userUID: firebaseUser.uid, data: pickedVideo, completed: { (downloadURL) in
                    if downloadURL == "error"{
                        GlobalActions.singleton.displayAlert(sender: self, title: "Error on video upload", message: "We could not upload your video at this time. Please try again later")
                    }else{
                        self.postImage.image = GlobalActions.singleton.generateThumbnail(url: NSURL(string: downloadURL)!, fromTime: 0)
                        self.downloadURL = downloadURL
                        self.type = "video"
                        self.submitButton.isEnabled = true
                    }
                })
            }
            dismiss(animated: true, completion: {
                self.blurView()
            })
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    

}
