//
//  KFCurriculumMenu.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class KFCurriculumMenuVC: UIViewController{
    //MARK: Properties
    var selectedColor: String!
    
    //MARK: IBActions
    @IBAction func techniquesButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "Techniques", sender: nil)
    }
    
    @IBAction func formsButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "Forms", sender: nil)
    }
    
    
    @IBAction func weaponsButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "Weapons", sender: nil)
    }
    
    @IBAction func selfDefenseButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "SelfDefense", sender: nil)
    }

    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Techniques"{
            let destVC = segue.destination as! KFCurriculumVideosVC
            destVC.selectedColor = self.selectedColor
            destVC.selectedCategory = "Techniques"
        }
        if segue.identifier == "Forms"{
            let destVC = segue.destination as! KFCurriculumVideosVC
            destVC.selectedColor = self.selectedColor
            destVC.selectedCategory = "Forms"
        }
        if segue.identifier == "Weapons"{
            let destVC = segue.destination as! KFCurriculumVideosVC
            destVC.selectedColor = self.selectedColor
            destVC.selectedCategory = "Weapons"
        }
        if segue.identifier == "SelfDefense"{
            let destVC = segue.destination as! KFCurriculumVideosVC
            destVC.selectedColor = self.selectedColor
            destVC.selectedCategory = "Self Defense"
        }
    }
   
}
