//
//  VideoData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class VideoData{
 
    private var _videoTitle: String!
    private var _videoURL: String!
    private var _videoImage: UIImage!
    
    var videoTitle: String{
        return _videoTitle
    }
    var videoURL: String{
        return _videoURL
    }
    var videoImage: UIImage{
        self._videoImage = GlobalActions.singleton.generateThumbnail(url: URL(string: videoURL)! as NSURL, fromTime: 0)
        return self._videoImage
    }
    
    init(videoTitle: String, videoURL: String){
        self._videoTitle = videoTitle
        self._videoURL = videoURL
    }
}
