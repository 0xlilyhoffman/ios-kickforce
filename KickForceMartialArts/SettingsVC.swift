//
//  SettingsVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/6/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class SettingsVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    //MARK: Properties
    var user: UserData!
    let imagePicker = UIImagePickerController()
    var beltColorPickerValues = ["White", "Orange", "Yellow", "Camo", "Green", "Purple", "Blue", "Brown","Red", "Black"]
    var selectedColor: String! = "White"
    var profilePicURL: String!
    
    //MARK: IBOutlets
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var pickerView: UIPickerView!
    
    //MARK: IBActions
    @IBAction func editImageButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            if (UIImagePickerController.availableMediaTypes(for: .photoLibrary)!).contains("public.image"){
                imagePicker.mediaTypes = ["public.image"]
            }
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Photo Library Access Error", message: "Cannot access photo library")
        }
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        pickerView.delegate = self
        pickerView.dataSource = self
        
        
        firstNameTextField.text = user.firstName
        lastNameTextField.text = user.lastName
        usernameTextField.text = user.username
        emailTextField.text = user.email
        profileImage.image = user.profilePic
        profilePicURL = user.profilePicURL
        self.selectedColor = user.beltColor
        
        
    }
    @IBAction func signOutPressed(_ sender: Any) {
        AuthService.singleton.signOut(sender: self, completed: {
            self.performSegue(withIdentifier: "SignOut", sender: nil)
        })
    }
    

    @IBAction func submitChangesButtonPressed(_ sender: Any) {
        updateFirebaseUser()
        DataService.singleton.submitUserUpdate(userUID: user.uid, firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, username: usernameTextField.text!, email: emailTextField.text!, beltColor: selectedColor, profilePic: profilePicURL, completed: {
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    //TODO: Move to AuthService
    func updateFirebaseUser(){
        if passwordTextField.text != nil && passwordTextField.text != ""{
            Auth.auth().currentUser?.updatePassword(to: passwordTextField.text!, completion: nil)
        }
        if emailTextField.text != nil && emailTextField.text != ""{
            Auth.auth().currentUser?.updateEmail(to: emailTextField.text!, completion: nil)
        }
    }

    //MARK: PickerView
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return beltColorPickerValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return beltColorPickerValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedColor = beltColorPickerValues[row]
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        let data = beltColorPickerValues[row]
        let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 16.0, weight: UIFontWeightLight)])
        
        label?.attributedText = title
        label?.font = UIFont.systemFont(ofSize: 16)
        label?.textAlignment = .center
        return label!
    }
    
    //MARK: ImagePicker
    //Upload image/video in Firebase storage and get url to put in database upon "submit"
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
      
        picker.allowsEditing = true
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            profileImage.image = pickedImage
            let data = UIImageJPEGRepresentation(pickedImage, 1.0)! as NSData
            DataService.singleton.uploadProfilePhotoPostToStorage(userUID: user.uid, data: data, completed: { (downloadURL) in
                if downloadURL == "error"{
                    GlobalActions.singleton.displayAlert(sender: self, title: "Error on photo upload", message: "We could not upload your photo at this time. Please try again later")
                }else{
                    self.profilePicURL = downloadURL
                    self.profileImage.image = pickedImage
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
}
