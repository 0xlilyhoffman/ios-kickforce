//
//  UserProfilePostVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class UserProfilePostVC: UIViewController{
    
    //MARK: Properties
    var presentingVC: UIViewController!
    var photo: ProfilePhotoData!
    
    //MARK: IBOutlets
    @IBOutlet var rootView: UIView!
    @IBOutlet var postImage: UIImageView!
    @IBOutlet var caption: UILabel!
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postImage.image = photo.image
        caption.text = photo.caption
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
    }
    
    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        removeView()
    }
    
    //Removes background blur from presenting VC
    func removeView(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }

    
    
}
