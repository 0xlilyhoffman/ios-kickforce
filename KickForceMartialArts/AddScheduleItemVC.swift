//
//  AddScheduleItemVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class AddScheduleItemVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource{
    
    //MARK: Properties
    var datePickerValues = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    var selectedDay: String! = "Sunday"
    var selectedTime: String! = "10:01 AM"
    var presentingVC: UIViewController!
    
    //MARK: IBOutlets
    @IBOutlet var rootView: UIView!
    @IBOutlet var classTitleTextField: UITextField!
    @IBOutlet var dayPickerView: UIPickerView!
    @IBOutlet var timePickerView: UIDatePicker!
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: IBActions
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        if classTitleTextField.text == nil || classTitleTextField.text == ""{
            classTitleTextField.text = " "
        }
        
        DataService.singleton.submitScheduleItem(day: selectedDay, name: classTitleTextField.text!, time: selectedTime){
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dayPickerView.delegate = self
        dayPickerView.dataSource = self
        timePickerView.addTarget(self, action: #selector(timePickerViewHandler), for: .valueChanged)
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
        
        addTapToDismissKeyboard()
    }
    
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }

    //MARK: PickerView
    func timePickerViewHandler(sender: UIDatePicker) {
        let timeFormatter = DateFormatter()
        timeFormatter.timeStyle = .short
        
        let strDate = timeFormatter.string(from: timePickerView.date)
        self.selectedTime = strDate
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return datePickerValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return datePickerValues[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedDay = datePickerValues[row]
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        let data = datePickerValues[row]
        let title = NSAttributedString(string: data, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightLight)])
        
        label?.attributedText = title
        label?.font = UIFont.systemFont(ofSize: 14)
        label?.textAlignment = .center
        return label!
    }
    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        removeView()
    }
    
    func removeView(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }

    
}
