//
//  UserProfileVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class UserProfileVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties
    static var photoCache: NSCache<NSString, ProfilePhotoData> = NSCache()
    var user: UserData!
    var photos = [ProfilePhotoData]()
    var colorImageMap = ["White": UIColor.white, "Orange": UIColor.orange, "Yellow": UIColor.yellow, "Camo": UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 0.0, alpha: 1.0), "Green": UIColor.green, "Purple": UIColor.purple, "Blue": UIColor.blue, "Brown": UIColor.brown, "Red": UIColor.red, "Black": UIColor.black]

    
    //MARK: IBOutlets
    @IBOutlet var profileImage: CircleImage!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var bioLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var headerView: RoundedView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    @IBOutlet var topView: RoundedView!
    
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        DataService.singleton.getUserData(userUID: user.uid, completed: { (user) in
            self.user = user
            self.nameLabel.text = user.name
            self.usernameLabel.text = "@" + user.username
            self.profileImage.image = user.profilePic
            self.profileImage.layer.borderWidth = 5.0
            self.profileImage.layer.borderColor = self.colorImageMap[user.beltColor]?.cgColor
            self.topView.layer.borderWidth = 5.0
            self.topView.layer.borderColor = self.colorImageMap[user.beltColor]?.cgColor

        })
        
        DataService.singleton.getProfilePhotos(userUID: user.uid, completed: { (photos) in
            self.photos = photos
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
        
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewPost"{
            let destVC = segue.destination as! UserProfilePostVC
            destVC.presentingVC = self
            destVC.photo = sender as! ProfilePhotoData
        }
        if segue.identifier == "PlayUserProfileVideo"{
            let destVC = segue.destination as! VideoPlayerVC
            let videoURL = NSURL(string: sender as! String)
            destVC.videoURL = videoURL
        }
    }
    
    //MARK: UICollectionViewDataSource, UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        if let photo = UserProfileVC.photoCache.object(forKey: photos[indexPath.row].mediaURL as NSString){
            photos[indexPath.row] = photo
            cell.postImage.image = photo.image
        }else{
            cell.configureCell(photoData: photos[indexPath.row], cache: UserProfileVC.photoCache)
        }
        if photos[indexPath.row].type == "video"{
            let cameraImage = UIImage(named: "ic_videocam")
            let imageView = UIImageView(image: cameraImage)
            imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            cell.addSubview(imageView)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if photos[indexPath.row].type == "image"{
            let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
            blurEffectView.frame = self.view.frame
            self.view.addSubview(blurEffectView)
        
            performSegue(withIdentifier: "ViewPost", sender: photos[indexPath.row])
        }
        
        if photos[indexPath.row].type == "video"{
            performSegue(withIdentifier: "PlayUserProfileVideo", sender: photos[indexPath.row].mediaURL)
        }
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 36) / 3
        return CGSize(width: side, height: side)
    }
    
}
