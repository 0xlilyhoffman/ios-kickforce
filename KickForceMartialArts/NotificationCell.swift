//
//  NotificationCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class NotificationCell: UITableViewCell{
    @IBOutlet var senderNameLabel: UILabel!
    @IBOutlet var postTextLabel: UILabel!
    @IBOutlet var timestampLabel: UILabel!
    
    
    func configureCell(notification: NotificationData){
        senderNameLabel.text = notification.senderName
        postTextLabel.text = notification.notificationText
        timestampLabel.text = notification.dateString
    }
}
