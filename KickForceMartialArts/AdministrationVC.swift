//
//  AdministrationVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class AdministrationVC: UIViewController{
    var presentingVC: UITableViewController!
    var adminCode: String!
    
    @IBOutlet var administratorCodeTextField: RoundedTextField!
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        DataService.singleton.getAdminCode(completed: { (code) in
            self.adminCode = code
            
            if self.administratorCodeTextField.text == self.adminCode{
                self.performSegue(withIdentifier: "AdminAccessGranted", sender: nil)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        })
        
    }
    
    @IBOutlet var rootView: UIView!
    
    

    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
        
        addTapToDismissKeyboard()
    }
    
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeView()
    }
    
    func removeView(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }

}
