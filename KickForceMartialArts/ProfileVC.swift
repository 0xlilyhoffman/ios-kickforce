//
//  ProfileVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class ProfileVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties
    var firebaseUser: User!
    var user: UserData!
    var photos = [ProfilePhotoData]()
    static var photoCache: NSCache<NSString, ProfilePhotoData> = NSCache()
    var colorImageMap = ["White": UIColor.white, "Orange": UIColor.orange, "Yellow": UIColor.yellow, "Camo": UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 0.0, alpha: 1.0), "Green": UIColor.green, "Purple": UIColor.purple, "Blue": UIColor.blue, "Brown": UIColor.brown, "Red": UIColor.red, "Black": UIColor.black]


    //MARK: IBOutlets
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var backgroundImage: UIImageView!
    @IBOutlet var profileImage: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var topView: RoundedView!
    
    //MARK: IBActions
    @IBAction func settingsButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "Settings", sender: nil)
    }
    @IBAction func notificationsButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "Notifications", sender: nil)
    }
    @IBAction func newPostButtonPressed(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        
        performSegue(withIdentifier: "NewPost", sender: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        firebaseUser = Auth.auth().currentUser
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        DataService.singleton.getUserData(userUID: firebaseUser.uid, completed: { (user) in
            self.user = user
            self.nameLabel.text = user.name
            self.usernameLabel.text = "@" + user.username
            self.profileImage.image = user.profilePic
            self.profileImage.layer.borderWidth = 5.0
            self.profileImage.layer.borderColor = self.colorImageMap[user.beltColor]?.cgColor
            self.topView.layer.borderWidth = 5.0
            self.topView.layer.borderColor = self.colorImageMap[user.beltColor]?.cgColor

        })
        
        DataService.singleton.getProfilePhotos(userUID: firebaseUser.uid, completed: { (photos) in
            self.photos = photos
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
        
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NewPost"{
            let destVC = segue.destination as! NewProfilePostVC
            destVC.presentingVC = self
        }
        if segue.identifier == "ViewPost"{
            let destVC = segue.destination as! ProfilePostVC
            destVC.presentingVC = self
            destVC.userUID = user.uid
            destVC.photo = sender as! ProfilePhotoData
        }
        if segue.identifier == "Settings"{
            let destVC = segue.destination as! SettingsVC
            destVC.user = user
        }
        if segue.identifier == "PlayProfileVideo"{
            let destVC = segue.destination as! VideoPlayerVC
            let videoURL = NSURL(string: sender as! String)
            destVC.videoURL = videoURL
        }
    }
    
    //MARK: UICollectionViewDataSource, UICollectionViewDelegate,
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos.count
    }
    
    //Configures cells: images are cached as [url:image].
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        
        //Search for cached images
        if let photo = ProfileVC.photoCache.object(forKey: photos[indexPath.row].mediaURL as NSString){
            photos[indexPath.row] = photo
            cell.postImage.image = photo.image
        }else{
            cell.configureCell(photoData: photos[indexPath.row], cache: ProfileVC.photoCache)
        }
        //Display video icon on video posts
        if photos[indexPath.row].type == "video"{
            let cameraImage = UIImage(named: "ic_videocam")
            let imageView = UIImageView(image: cameraImage)
            imageView.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
            cell.addSubview(imageView)
        }
        return cell
    }
    
    //When an image is selected, screen is blurred and a popup view of the image appears
    //When a video is selected, AVPlayerVC is presented to play the video
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if photos[indexPath.row].type == "image"{
            let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
            blurEffectView.frame = self.view.frame
            self.view.addSubview(blurEffectView)
            performSegue(withIdentifier: "ViewPost", sender: photos[indexPath.row])
        }
        if photos[indexPath.row].type == "video"{
            determineDeleteOrWatch(indexPath: indexPath)
        }
    }
    
    //Presents a pop up to the user asking if they want to delete or watch their video
    func determineDeleteOrWatch(indexPath: IndexPath){
        let errorAlert = UIAlertController(title: "Delete or Watch?", message: "", preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
            DataService.singleton.deleteProfilePhoto(userUID: self.user.uid, photoRef: self.photos[indexPath.row].mediaReference)
            self.refreshPhotos()
        })
        
        let dismiss = UIAlertAction(title: "View", style: .cancel, handler: { (action) -> Void in
            self.performSegue(withIdentifier: "PlayProfileVideo", sender: self.photos[indexPath.row].mediaURL)

        })
        errorAlert.addAction(delete)
        errorAlert.addAction(dismiss)
        
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    //Reloads photos (called after deleting a video) to reflect changes
    func refreshPhotos(){
        DataService.singleton.getProfilePhotos(userUID: user.uid, completed: { (photos) in
            self.photos = photos
            self.collectionView.reloadData()
        })
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    //Sizes CollectionViewCells so that 3 columns fit on screen
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 36) / 3
        return CGSize(width: side, height: side)
    }
    
    
    
}
