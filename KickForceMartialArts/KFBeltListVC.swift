//
//  KFBeltListVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class KFBeltListVC: UITableViewController{
   
    //MARK: Properties
    var selectedColor: String!
    var indexToBeltMap = [0: "White", 1: "Orange", 2: "Yellow", 3: "Camo", 4: "Green", 5: "Purple", 6: "Blue", 7: "Brown", 8: "Red", 9: "Black"]
    
    //MARK: UITableViewController
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedColor = indexToBeltMap[indexPath.row]
        performSegue(withIdentifier: "BeltColorSelected", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "BeltColorSelected"{
            let destVC = segue.destination as! KFCurriculumMenuVC
            destVC.selectedColor = selectedColor
        }
    }
}
