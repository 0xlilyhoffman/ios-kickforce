//
//  GlobalActions.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class GlobalActions{
    //MARK: GlobalActions Singleton
    private static let _singleton = GlobalActions()
    static var singleton: GlobalActions{
        return _singleton
    }
    
    var themeGreen = UIColor(red: 61.0/255.0, green: 191.0/255.0, blue: 61.0/255.0, alpha: 1.0)
    
    //Generates thumbnail image from video url
    func generateThumbnail(url : NSURL, fromTime:Float64) -> UIImage {
        let asset :AVAsset = AVAsset(url: url as URL)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        assetImgGenerate.requestedTimeToleranceAfter = kCMTimeZero;
        assetImgGenerate.requestedTimeToleranceBefore = kCMTimeZero;
        do{
            let time        : CMTime = CMTimeMakeWithSeconds(fromTime, 600)
            let img         : CGImage = try assetImgGenerate.copyCGImage(at: time, actualTime: nil)
            let frameImg = UIImage(cgImage: img)
            //var frameImg    : UIImage = UIImage(CGImage: img)!
            return frameImg
        }catch let error as NSError{
            print(error.localizedDescription)
        }
        return UIImage()
    }
    
    //Returns date string eg "Aug 13, 2017, 8:58 AM" from timestamp
    func getDateString(from timestamp: Double) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.timeStyle = .short
        dateFormatter.locale = Locale(identifier: "en_US")
        let date = Date(timeIntervalSince1970: timestamp)
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    //Returns an image given a url for that image
    func getImage(from mediaURL: String) -> UIImage{
        if let url = URL(string: mediaURL){
            if let data = NSData(contentsOf: url){
                let image = UIImage(data: data as Data)
                if image != nil{
                    return image!
                }
            }
        }
        return #imageLiteral(resourceName: "default_user")
    }
    
    //Display pop up alert to user
    func displayAlert(sender: UIViewController, title: String, message: String){
        let errorAlert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let dismissAlert = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in })
        errorAlert.addAction(dismissAlert)
        sender.present(errorAlert, animated: true, completion: nil)
    }
}
