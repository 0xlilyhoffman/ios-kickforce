//
//  WorkoutVideosVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class WorkoutVideosVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties
    var videos = [VideoData]()
    static var thumbnailCache: NSCache<NSString, UIImage> = NSCache()

    //MARK: IBOutlets
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()

        collectionView.delegate = self
        collectionView.dataSource = self
        
        DataService.singleton.getWorkoutVideos(completed: { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
    }
    
    //MARK: UICollectionViewDataSource, UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        
        if let thumbnail = WorkoutVideosVC.thumbnailCache.object(forKey: videos[indexPath.row].videoURL as NSString){
            cell.videoImage.image = thumbnail
            cell.configureCell(videoData: videos[indexPath.row], needImage: false, cache: WorkoutVideosVC.thumbnailCache)
        }else{
            cell.configureCell(videoData: videos[indexPath.row], needImage: true, cache: WorkoutVideosVC.thumbnailCache)
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "PlayWorkoutVideo", sender: videos[indexPath.row].videoURL)
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 30) / 2
        return CGSize(width: side, height: side + 60)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "PlayWorkoutVideo"{
            let destVC = segue.destination as! VideoPlayerVC
            let videoURL = NSURL(string: sender as! String)
            destVC.videoURL = videoURL
        }
    }
}
