//
//  NewsfeedCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class NewsfeedCell: UITableViewCell{

    var tapAction: ((UITableViewCell) -> Void)?

    @IBOutlet var authorButton: UIButton!
    @IBAction func authorButtonPressed(_ sender: Any) {
        tapAction?(self)
    }
    
    @IBOutlet var authorImage: UIImageView!
    @IBOutlet var postTextLabel: UILabel!
    @IBOutlet var timestampLabel: UILabel!
    

    func configureCell(post: NewsfeedData, needPhoto: Bool){
        authorButton.setTitle(post.postAuthor, for: .normal)
        postTextLabel.text = post.postText
        timestampLabel.text = post.dateString
        
        if needPhoto == true{
            //pull image from internet
            let img = post.postAuthorPic
            authorImage.image = img
            
            //add image to cache
            NewsfeedVC.newsfeedCache.setObject(img, forKey: post.postAuthorPicURL as NSString)
        }
    }
}
