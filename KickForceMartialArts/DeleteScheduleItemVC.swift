//
//  DeleteScheduleItemVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class DeleteScheduleItemVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    //MARK: Properties
    var monday = [ScheduleData]()
    var tuesday = [ScheduleData]()
    var wednesday = [ScheduleData]()
    var thursday = [ScheduleData]()
    var friday = [ScheduleData]()
    var saturday = [ScheduleData]()
    var sunday = [ScheduleData]()
    var sectionHeaderTitles = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

    //MARK: IBOutlets
    @IBOutlet var tableView: UITableView!

    //MARK: IBActions
    @IBAction func addButtonTapped(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        
        performSegue(withIdentifier: "AddScheduleItem", sender: nil)
    }

    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        self.getSchedule(completed: {
            self.tableView.reloadData()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getSchedule(completed: {
            self.tableView.reloadData()
        })
    }

    
    //pull schedule fom firebase via DataService
    func getSchedule(completed: @escaping () -> ()){
        DataService.singleton.getDayClasses(day: "Monday", completed: { (day) in
            self.monday = day
            self.tableView.reloadData()
        })
        DataService.singleton.getDayClasses(day: "Tuesday", completed: { (day) in
            self.tuesday = day
            self.tableView.reloadData()
        })
        DataService.singleton.getDayClasses(day: "Wednesday", completed: { (day) in
            self.wednesday = day
            self.tableView.reloadData()
        })
        DataService.singleton.getDayClasses(day: "Thursday", completed: { (day) in
            self.thursday = day
            self.tableView.reloadData()
        })
        DataService.singleton.getDayClasses(day: "Friday", completed: { (day) in
            self.friday = day
            self.tableView.reloadData()
        })
        DataService.singleton.getDayClasses(day: "Saturday", completed: { (day) in
            self.saturday = day
            self.tableView.reloadData()
        })
        DataService.singleton.getDayClasses(day: "Sunday", completed: { (day) in
            self.sunday = day
            self.tableView.reloadData()
        })
        completed()
    }
    
    //MARK: ToggleSelection
    //set boolean flag on ScheduleData object
    func toggleDoneButton(day: [ScheduleData], cell: ScheduleCell, indexPath: IndexPath){
        if day[indexPath.row].selected == false{
            day[indexPath.row].selected = true
        }else{
            day[indexPath.row].selected = false
        }
    }
    
    //change image of selected/unselected item 
    func setDoneButton(day: [ScheduleData], cell: ScheduleCell, indexPath: IndexPath){
        if day[indexPath.row].selected == true{
            cell.toggleButton.setImage(UIImage(named: "ic_refresh"), for: .normal)
        }else{
            cell.toggleButton.setImage(UIImage(named: "ic_delete"), for: .normal)
        }
    }
    
    //MARK: UITableViewDelegate, UITableViewDataSource
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleCell", for: indexPath) as! ScheduleCell
        
        
        switch(indexPath.section){
        case 0:
            cell.classNameLabel.text = monday[indexPath.row].title
            cell.timeLabel.text = monday[indexPath.row].time
            setDoneButton(day: monday, cell: cell, indexPath: indexPath)
        case 1:
            cell.classNameLabel.text = tuesday[indexPath.row].title
            cell.timeLabel.text = tuesday[indexPath.row].time
            setDoneButton(day: tuesday, cell: cell, indexPath: indexPath)
        case 2:
            cell.classNameLabel.text = wednesday[indexPath.row].title
            cell.timeLabel.text = wednesday[indexPath.row].time
            setDoneButton(day: wednesday, cell: cell, indexPath: indexPath)
        case 3:
            cell.classNameLabel.text = thursday[indexPath.row].title
            cell.timeLabel.text = thursday[indexPath.row].time
            setDoneButton(day: thursday, cell: cell, indexPath: indexPath)
        case 4:
            cell.classNameLabel.text = friday[indexPath.row].title
            cell.timeLabel.text = friday[indexPath.row].time
            setDoneButton(day: friday, cell: cell, indexPath: indexPath)
        case 5:
            cell.classNameLabel.text = saturday[indexPath.row].title
            cell.timeLabel.text = saturday[indexPath.row].time
            setDoneButton(day: saturday, cell: cell, indexPath: indexPath)
        case 6:
            cell.classNameLabel.text = sunday[indexPath.row].title
            cell.timeLabel.text = sunday[indexPath.row].time
            setDoneButton(day: sunday, cell: cell, indexPath: indexPath)
            
        default:
            cell.classNameLabel.text = " "
            cell.timeLabel.text = " "
        }
        cell.tapAction = {(cell) in
            let cell = cell as! ScheduleCell
            
            switch(indexPath.section){
            case 0: self.toggleDoneButton(day: self.monday, cell: cell, indexPath: indexPath)
            case 1: self.toggleDoneButton(day: self.tuesday, cell: cell, indexPath: indexPath)
            case 2: self.toggleDoneButton(day: self.wednesday, cell: cell,  indexPath: indexPath)
            case 3: self.toggleDoneButton(day: self.thursday,  cell: cell, indexPath: indexPath)
            case 4: self.toggleDoneButton(day: self.friday,  cell: cell, indexPath: indexPath)
            case 5: self.toggleDoneButton(day: self.saturday, cell: cell,  indexPath: indexPath)
            case 6: self.toggleDoneButton(day: self.sunday, cell: cell,  indexPath: indexPath)
            default: print(" ")
            }
            
            tableView.reloadData()
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionHeaderTitles.count
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionHeaderTitles[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch(section){
        case 0: return monday.count
        case 1: return tuesday.count
        case 2: return wednesday.count
        case 3: return thursday.count
        case 4: return friday.count
        case 5: return saturday.count
        case 6: return sunday.count
        default: return 0
        }
    }
    func tableView(_: UITableView, willDisplay cell: UITableViewCell, forRowAt: IndexPath){
        cell.backgroundColor = .clear
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        label.textAlignment = .center
        label.backgroundColor = .black
        label.text = sectionHeaderTitles[section]
        label.textColor = .white
        return label
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddScheduleItem"{
            let destVC = segue.destination as! AddScheduleItemVC
            destVC.presentingVC = self
        }
    }
    
    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        deleteSelectedClasses(day: self.monday, dayString: "Monday")
        deleteSelectedClasses(day: self.tuesday, dayString: "Tuesday")
        deleteSelectedClasses(day: self.wednesday, dayString: "Wednesday")
        deleteSelectedClasses(day: self.thursday, dayString: "Thursday")
        deleteSelectedClasses(day: self.friday, dayString: "Friday")
        deleteSelectedClasses(day: self.saturday, dayString: "Saturday")
        deleteSelectedClasses(day: self.sunday, dayString: "Sunday")
    }
    func deleteSelectedClasses(day: [ScheduleData], dayString: String){
        for item in day{
            if item.selected == true{
                DataService.singleton.deleteClass(day: dayString, title: item.title)
            }
        }
    }
}
