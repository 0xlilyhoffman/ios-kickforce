//
//  StudentsListVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class StudentsListVC: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    //MARK: Properties
    var users = [UserData]()

    //MARK: IBOutlets
    @IBOutlet var tableView: UITableView!
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        DataService.singleton.getAllUsers(completed: { (users) in
            self.users = users
            self.tableView.reloadData()
        })
    }
    
    //MARK: UITableViewDelegate / UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = users[indexPath.row].name
        cell.detailTextLabel?.text = users[indexPath.row].username
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ViewUserProfile", sender: users[indexPath.row])
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ViewUserProfile"{
            let destVC = segue.destination as! UserProfileVC
            destVC.user = sender as! UserData
        }
    }
    
}
