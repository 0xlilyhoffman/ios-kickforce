//
//  AddStoreItemVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class AddStoreItemVC: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    //MARK: Properties
    var presentingVC: UIViewController!
    var downloadURL: String! = "error"
    let imagePicker = UIImagePickerController()
    
    //MARK: IBOutlet
    @IBOutlet var rootView: UIView!
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var priceTextField: UITextField!
    @IBOutlet var itemImage: UIImageView!
    
    //MARK: IBAcitons
    @IBAction func cameraButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = true
            if (UIImagePickerController.availableMediaTypes(for: .photoLibrary)!).contains("public.image"){
                imagePicker.mediaTypes = ["public.image"]
            }
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            GlobalActions.singleton.displayAlert(sender: self, title: "Photo Library Access Error", message: "Cannot access photo library")
        }

        
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.removeBlur()
        })
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        
        if titleTextField.text == nil || titleTextField.text == ""{
            titleTextField.text = " "
        }
        if priceTextField.text == nil || priceTextField.text == ""{
            priceTextField.text = " "
        }
        
        DataService.singleton.submitStoreItem(title: titleTextField.text!, price: priceTextField.text!, imageURL: downloadURL)
        
        dismiss(animated: true, completion: {
            self.removeBlur()
        })
        
    }

    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self

        rootView.layer.cornerRadius = 15.0
        rootView.layer.masksToBounds = true
        
        priceTextField.text = "$"
        
        addTapToDismissKeyboard()
    }
    
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }

    
    //MARK: PickerView
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.allowsEditing = true
        if let pickedImage = info[UIImagePickerControllerEditedImage] as? UIImage{
            itemImage.image = pickedImage
            let data = UIImageJPEGRepresentation(pickedImage, 1.0)! as NSData
            
            DataService.singleton.uploadStoreItemToStorage(imageData: data, completed: { (downloadURL) in
                if downloadURL == "error"{
                    GlobalActions.singleton.displayAlert(sender: self, title: "Error on photo upload", message: "We could not upload your photo at this time. Please try again later")
                }else{
                    self.downloadURL = downloadURL
                    self.itemImage.image = pickedImage
                }
                self.blurView()
                self.dismiss(animated: true, completion: {
                    self.blurView()
                    self.itemImage.image = pickedImage
                })
            
            })
            
        }
    }
    
    //MARK: Close
    override func viewWillDisappear(_ animated: Bool) {
        removeBlur()
    }
    
    func blurView(){
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = presentingVC.view.frame
        presentingVC.view.addSubview(blurEffectView)
    }
    func removeBlur(){
        for subview in self.presentingVC.view.subviews {
            if subview is UIVisualEffectView {
                subview.removeFromSuperview()
            }
        }
    }

}
