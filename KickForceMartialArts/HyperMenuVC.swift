//
//  HyperMenuVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class HyperMenuVC: UIViewController{
    
    //MARK: IBActions
    @IBAction func levelOneKicksButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelOneKicks", sender: nil)
    }
    
    @IBAction func levelOneTricksButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelOneTricks", sender: nil)
    }
    
    @IBAction func levelOneHandCombosButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelOneHandCombos", sender: nil)
    }
    
    @IBAction func levelOneWeaponsButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelOneWeapons", sender: nil)
    }
    
    @IBAction func levelTwoKicksButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelTwoKicks", sender: nil)
    }
    
    @IBAction func levelTwoTricksButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelTwoTricks", sender: nil)
    }
    
    @IBAction func levelTwoHandCombosButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelTwoHandCombos", sender: nil)
    }
    
    @IBAction func levelTwoWeaponsButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "LevelTwoWeapons", sender: nil)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "LevelOneKicks"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "One"
            destVC.category = "Kicks"
        }
        if segue.identifier == "LevelOneTricks"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "One"
            destVC.category = "Tricks"
        }
        if segue.identifier == "LevelOneHandCombos"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "One"
            destVC.category = "Hand Combos"
        }
        if segue.identifier == "LevelOneWeapons"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "One"
            destVC.category = "Weapons"
        }
        if segue.identifier == "LevelTwoKicks"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "Two"
            destVC.category = "Kicks"
        }
        if segue.identifier == "LevelTwoTricks"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "Two"
            destVC.category = "Tricks"
        }
        if segue.identifier == "LevelTwoHandCombos"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "Two"
            destVC.category = "Hand Combos"
        }
        if segue.identifier == "LevelTwoWeapons"{
            let destVC = segue.destination as! HyperVideosVC
            destVC.level = "Two"
            destVC.category = "Weapons"
        }
    }   
}

