//
//  DeleteWorkoutVideoVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class DeleteWorkoutVideoVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    //MARK: Properties
    var videos = [VideoData]()
    static var thumbnailCache: NSCache<NSString, UIImage> = NSCache()


    //MARK: IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!

    
    //MARK: IBActions
    @IBAction func addButtonPressed(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        
        performSegue(withIdentifier: "AddWorkoutVideo", sender: nil)
    }
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()

        
        DataService.singleton.getWorkoutVideos(completed: { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
        
    }
    
    //MARK: CollectionView
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    /*
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        cell.configureCell(videoData: videos[indexPath.row])
        return cell
    }*/
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        
        if let thumbnail = DeleteWorkoutVideoVC.thumbnailCache.object(forKey: videos[indexPath.row].videoURL as NSString){
            cell.videoImage.image = thumbnail
            cell.configureCell(videoData: videos[indexPath.row], needImage: false, cache: DeleteWorkoutVideoVC.thumbnailCache)
        }else{
            cell.configureCell(videoData: videos[indexPath.row], needImage: true, cache: DeleteWorkoutVideoVC.thumbnailCache)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 30) / 2
        return CGSize(width: side, height: side + 60)
    }
    

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        confirmDelete(indexPath: indexPath)
        
    }
    
    func confirmDelete(indexPath: IndexPath){
        let errorAlert = UIAlertController(title: "Confirm Delete", message: "", preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
            DataService.singleton.deleteWorkoutVideo(videoTitle: self.videos[indexPath.row].videoTitle, completed: {
                self.videos.remove(at: indexPath.row)
                self.collectionView.reloadData()
            })
        })
        
        let dismiss = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        errorAlert.addAction(delete)
        errorAlert.addAction(dismiss)
        
        self.present(errorAlert, animated: true, completion: nil)
    }
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddWorkoutVideo"{
            let destVC = segue.destination as! AddWorkoutVC
            destVC.presentingVC = self
        }
    }
    
    
}
