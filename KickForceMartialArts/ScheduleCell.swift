//
//  ScheduleCell.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class ScheduleCell: UITableViewCell{  
    var tapAction: ((UITableViewCell) -> Void)?

    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var classNameLabel: UILabel!
    @IBAction func toggleButtonPressed(_ sender: Any) {
        tapAction?(self)
    }
    @IBOutlet var toggleButton: UIButton!
    
    
    func configureCell(scheduleItem: ScheduleData){
        timeLabel.text = scheduleItem.time
        classNameLabel.text = scheduleItem.title
    }
}
