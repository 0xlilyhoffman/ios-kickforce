//
//  NewsfeedViewInner.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/9/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class NewsfeedViewInner: UIView{
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.masksToBounds = true
        layer.cornerRadius = 15.0
        self.backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.25)
    }
    
}
