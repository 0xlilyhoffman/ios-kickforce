//
//  DeleteHyperVideoVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/7/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class DeleteHyperVideoVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIPickerViewDelegate, UIPickerViewDataSource{
    
    //MARK: Properties
    var level: String! = "One"
    var category: String! = "Kicks"
    var levelPickerValues = ["One", "Two"]
    var categoryPickerValues = ["Kicks", "Tricks", "Hand Combos", "Weapons"]
    var videos = [VideoData]()
    static var thumbnailCache: NSCache<NSString, UIImage> = NSCache()


    //MARK: IBOutlet
    @IBOutlet var levelPicker: UIPickerView!
    @IBOutlet var categoryPicker: UIPickerView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    
    //MARK: IBActions
    @IBAction func addButtonPressed(_ sender: Any) {
        let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
        blurEffectView.frame = self.view.frame
        self.view.addSubview(blurEffectView)
        
        performSegue(withIdentifier: "AddHyperVideo", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        levelPicker.delegate = self
        levelPicker.dataSource = self
        categoryPicker.delegate = self
        categoryPicker.dataSource = self
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()

        DataService.singleton.getHyperVideos(level: level, category: category, completed: { (videos) in
            self.videos = videos
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
        
        
    }
    
    //MARK: UICollectionViewDataSource, UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return videos.count
    }
    /*
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        cell.configureCell(videoData: videos[indexPath.row])
        return cell
    }*/
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        if let thumbnail = DeleteHyperVideoVC.thumbnailCache.object(forKey: videos[indexPath.row].videoURL as NSString){
            cell.configureCell(videoData: videos[indexPath.row], needImage: false, cache: DeleteHyperVideoVC.thumbnailCache)
            cell.videoImage.image = thumbnail
            
        }else{
            cell.configureCell(videoData: videos[indexPath.row], needImage: true, cache: DeleteHyperVideoVC.thumbnailCache)
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        confirmDelete(indexPath: indexPath)
        
    }
    
    //MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 30) / 2
        return CGSize(width: side, height: side + 60)
    }
    
    func confirmDelete(indexPath: IndexPath){
        let errorAlert = UIAlertController(title: "Confirm Delete", message: "", preferredStyle: .actionSheet)
        let delete = UIAlertAction(title: "Delete", style: .destructive, handler: { (action) -> Void in
            DataService.singleton.deleteHyperVideo(level: self.level, category: self.category, videoTitle: self.videos[indexPath.row].videoTitle) {
                self.videos.remove(at: indexPath.row)
                self.collectionView.reloadData()
            }
        })
    
        let dismiss = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        errorAlert.addAction(delete)
        errorAlert.addAction(dismiss)
        
        self.present(errorAlert, animated: true, completion: nil)
    }
    
    //MARK: PickerView
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == levelPicker{
            return levelPickerValues.count
        }
        if pickerView == categoryPicker{
            return categoryPickerValues.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == levelPicker{
            return levelPickerValues[row]
        }
        if pickerView == categoryPicker{
            return categoryPickerValues[row]
        }
        return " "
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == levelPicker{
            level = levelPickerValues[row]
            activitySpinner.startAnimating()
            DataService.singleton.getHyperVideos(level: level, category: category, completed: { (videos) in
                self.videos = videos
                self.collectionView.reloadData()
                self.activitySpinner.stopAnimating()
                
            })
        }
        if pickerView == categoryPicker{
            category = categoryPickerValues[row]
            activitySpinner.startAnimating()
            DataService.singleton.getHyperVideos(level: level, category: category, completed: { (videos) in
                self.videos = videos
                self.collectionView.reloadData()
                self.activitySpinner.stopAnimating()
            })
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        var label = view as! UILabel!
        if label == nil {
            label = UILabel()
        }
        
        if pickerView == levelPicker{
            let colorData = levelPickerValues[row]
            let title = NSAttributedString(string: colorData, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightLight)])
            label?.attributedText = title
            label?.font = UIFont.systemFont(ofSize: 14)
            label?.textAlignment = .center
            return label!
        }
        if pickerView == categoryPicker{
            let categoryData = categoryPickerValues[row]
            let title = NSAttributedString(string: categoryData, attributes: [NSFontAttributeName: UIFont.systemFont(ofSize: 14.0, weight: UIFontWeightLight)])
            
            
            label?.attributedText = title
            label?.font = UIFont.systemFont(ofSize: 14)
            label?.textAlignment = .center
            return label!
        }
        
        return UIView()
    }
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AddHyperVideo"{
            let destVC = segue.destination as! AddHyperVideoVC
            destVC.presentingVC = self
            destVC.level = self.level
            destVC.category = self.category
        }
    }


}
