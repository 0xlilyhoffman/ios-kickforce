//
//  LoginVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class LoginVC: UIViewController{
    
    //MARK: IBOutlets
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var passwordTextfield: UITextField!
    
    //MARK: IBActions
    
    //Verify credentials and login
    @IBAction func signInButtonPressed(_ sender: Any) {
        if (emailTextField.text == nil || emailTextField.text == "" || passwordTextfield.text == nil || passwordTextfield.text == ""){
            GlobalActions.singleton.displayAlert(sender: self, title: "Insufficient Information", message: "Please enter email and password")
            return
        }
        
        AuthService.singleton.signIn(sender: self, email: emailTextField.text!, password: passwordTextfield.text!, completed: { (success, message) in
            if success{
                self.performSegue(withIdentifier: "Login", sender: nil)
            }
        })
    }
    
    //Send password recovery email
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        AuthService.singleton.forgotPassword(sender: self, email: emailTextField.text!)
        GlobalActions.singleton.displayAlert(sender: self, title: "Success", message: "Password recovery email was sent to \(emailTextField.text!)")
    }
    
    //goto - create account screen
    @IBAction func createAccountButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "CreateAccount", sender: nil)
    }
    
    //MARK: Init
    //Add tap to dismiss keyboard functionality
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapToDismissKeyboard()
    }
    
    func addTapToDismissKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
}
