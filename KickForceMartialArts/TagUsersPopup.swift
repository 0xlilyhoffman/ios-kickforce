//
//  TagUsersPopup.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/9/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit


protocol PopUpSenderDelegate: class{
    func dataTransfer(users: [PopUpSelectionData])
}



class TagUsersPopup: UIViewController, UITableViewDelegate, UITableViewDataSource{
    var users: [UserData]!
    var usersToSelect = [PopUpSelectionData]()
    var usersSelected =  [PopUpSelectionData]()
    
    
    weak var delegate: PopUpSenderDelegate?
    
    @IBOutlet var rootView: UIView!
    @IBOutlet var tableView: UITableView!
    
    
    
    @IBAction func doneButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: {
            for user in self.usersToSelect{
                if user.selected == true{
                    self.usersSelected.append(user)
                }
            }
     
            self.delegate?.dataTransfer(users: self.usersSelected)
        })
    }
    
    override func viewDidLoad() {
        tableView.delegate = self
        tableView.dataSource = self
        
        
        DataService.singleton.getAllUsers(completed: { (users) in
            self.users = users
            self.convertUsersToSelectionData(users: users)
            self.tableView.reloadData()
        })
        
    }
    
    func convertUsersToSelectionData(users: [UserData]){
        for user in users{
            let selection = PopUpSelectionData(uidRef: user.uid, title: user.firstName + " " + user.lastName, selected: false)
            usersToSelect.append(selection)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usersToSelect.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PopUpSelectionCell", for: indexPath) as! PopUpSelectionCell
        
        
        cell.configureCell(data: usersToSelect[indexPath.row].title, selected: usersToSelect[indexPath.row].selected)
        cell.tapAction = {(cell) in
            if self.usersToSelect[indexPath.row].selected == false{
                self.usersToSelect[indexPath.row].selected = true
            }else{
                self.usersToSelect[indexPath.row].selected = false
            }
            tableView.reloadData()
        }

        return cell
    }
    
}
