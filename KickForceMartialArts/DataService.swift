//
//  DataService.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/4/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import Firebase

let DB_BASE = Database.database().reference()
let STORAGE_BASE = Storage.storage().reference()

let USERS = "Users"

let HYPER = "HyperVideos"
let CURRICULUM = "KickForceCurriculum"
let WORKOUTS = "WorkoutVideos"

let SCHEDULE = "KickForceSchedule"

let NEWSFEED = "Newsfeed"
let PROFILE = "Profile"

let STORE = "Store"


class DataService{
    //MARK: DataService Singleton
    private static let _singleton = DataService()
    static var singleton: DataService{
        return _singleton
    }
    
    
    
    //MARK:----------------------------------------------------------------------------------
    //MARK: Database Entry Points
    //MARK:----------------------------------------------------------------------------------
    //Authentication
    var firebaseRef_AdministratorCode: DatabaseReference{
        return DB_BASE.child("General").child("AdministratorCode")
    }
    var firebaseRef_KickForceCode: DatabaseReference{
        return DB_BASE.child("General").child("KickForceCode")
    }
    
    //Videos
    var firebaseRef_Hyper: DatabaseReference{
        return DB_BASE.child(HYPER)
    }
    var firebaseRef_Curriculum: DatabaseReference{
        return DB_BASE.child(CURRICULUM)
    }
    var firebaseRef_Workout: DatabaseReference{
        return DB_BASE.child(WORKOUTS)
    }
    
    //KickForce data
    var firebaseRef_Schedule: DatabaseReference{
        return DB_BASE.child(SCHEDULE)
    }
    var firebaseRef_Store: DatabaseReference{
        return DB_BASE.child(STORE)
    }
    
    //Social
    var firebaseRef_Newsfeed: DatabaseReference{
        return DB_BASE.child(NEWSFEED)
    }
    var firebaseRef_Profile: DatabaseReference{
        return DB_BASE.child(PROFILE)
    }
    var firebaseRef_Users: DatabaseReference{
        return DB_BASE.child(USERS)
    }
    
    //MARK:----------------------------------------------------------------------------------
    //MARK: Storage Entry Points
    //MARK:----------------------------------------------------------------------------------    
    var firebaseRef_ProfilePosts: StorageReference{
        return STORAGE_BASE.child(PROFILE)
    }
    var firebaseRef_StorePosts: StorageReference{
        return STORAGE_BASE.child(STORE)
    }
    
    var firebaseRef_HyperVideos: StorageReference{
        return STORAGE_BASE.child(HYPER)
    }
    var firebaseRef_CurriculumVideos: StorageReference{
        return STORAGE_BASE.child(CURRICULUM)
    }
    var firebaseRef_WorkoutVideos: StorageReference{
        return STORAGE_BASE.child(WORKOUTS)
    }
    

    
    //MARK:----------------------------------------------------------------------------------
    //MARK: Submit To Firebase Database
    //MARK:----------------------------------------------------------------------------------
    //MARK: Administration Submissions
    func submitHyperVideo(level: String, category: String, title: String, url: String){
        firebaseRef_Hyper.child(level).child(category).child(title).setValue(url)
    }
    
    func submitCurriculumVideo(beltColor: String, category: String, title: String, url: String){
        firebaseRef_Curriculum.child(beltColor).child(category).child(title).setValue(url)
    }
    
    func submitWorkoutVideo(title: String, url: String){
        firebaseRef_Workout.child(title).setValue(url)
    }
    
    func submitScheduleItem(day: String, name: String, time: String, completed: @escaping () -> () ){
        firebaseRef_Schedule.child(day).child(name).setValue(time)
        completed()
    }
    
    func submitStoreItem(title: String, price: String, imageURL: String){
        let storeItem = ["title": title, "price": price, "imageURL": imageURL]
        firebaseRef_Store.childByAutoId().setValue(storeItem)
    }
    
    //MARK: User Submissions
    func submitNewUser(user: UserData, completed: @escaping () -> ()){
        let newUser = ["firstName": user.firstName, "lastName": user.lastName, "username": user.username, "email": user.email, "uid": user.uid, "profilePic": user.profilePicURL, "beltColor": user.beltColor] as [String : Any]
        firebaseRef_Users.child(user.uid).setValue(newUser)
        completed()
    }
    
    func submitUserUpdate(userUID: String, firstName: String, lastName: String, username: String, email: String, beltColor: String, profilePic: String, completed: @escaping ()  -> ()){
        firebaseRef_Users.child(userUID).child("firstName").setValue(firstName)
        firebaseRef_Users.child(userUID).child("lastName").setValue(lastName)
        firebaseRef_Users.child(userUID).child("username").setValue(username)
        firebaseRef_Users.child(userUID).child("email").setValue(email)
        firebaseRef_Users.child(userUID).child("beltColor").setValue(beltColor)
        firebaseRef_Users.child(userUID).child("profilePic").setValue(profilePic)
    }
    
    func submitProfilePostToDatabase(userUID: String, downloadURL: String, type: String, caption: String){
        let newPost = ["caption": caption, "photoURL": downloadURL, "type": type, "timestamp": Date().timeIntervalSince1970] as [String : Any]
        firebaseRef_Profile.child(userUID).child("Posts").childByAutoId().setValue(newPost)
    }
    
    func submitNewsfeedPost(postText: String, postAuthor: String, postAuthorUID: String, postAuthorPic: String, postTimestamp: Double){
        let newPost = ["postText": postText, "postAuthor": postAuthor, "postAuthorUID": postAuthorUID, "postAuthorPicURL": postAuthorPic, "timestamp": postTimestamp] as [String: Any]
        firebaseRef_Newsfeed.childByAutoId().setValue(newPost)
    }
    
    func submitNotification(receiverUID: String, text: String, senderName: String, timestamp: Double){
        let notification = ["senderName": senderName, "text": text, "timestamp": timestamp] as [String : Any]
        firebaseRef_Profile.child(receiverUID).child("Notifications").childByAutoId().setValue(notification)
    }
    
    func submitClassToUserSchedule(userUID: String, day: String, title: String, time: String){
        firebaseRef_Users.child(userUID).child("Schedule").child(day).child(title).setValue(time)
    }
    
    
    //MARK:----------------------------------------------------------------------------------
    //MARK: Upload To Firebase Storage
    //MARK:----------------------------------------------------------------------------------
    //Upload to Firebase Storage
    func uploadStoreItemToStorage(imageData: NSData, completed: @escaping (String) -> () ){
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        let unique_filename = NSUUID.init().uuidString
        
        firebaseRef_StorePosts.child(unique_filename).putData(imageData as Data, metadata: metaData, completion: { (metadata, error) in
            if error == nil{
                completed(metadata!.downloadURL()!.absoluteString)
            }else{
                completed("error")
            }
        })
    }
    
    func uploadProfilePhotoPostToStorage(userUID: String, data: NSData, completed: @escaping (String) -> ()){
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        let unique_filename = NSUUID.init().uuidString
        
        firebaseRef_ProfilePosts.child(userUID).child(unique_filename).putData(data as Data, metadata: metaData, completion: { (metadata, error) in
            if error != nil {
                completed("error")
            }
            else{
                completed(metadata!.downloadURL()!.absoluteString)
            }
        })
    }
    
    func uploadProfileVideoPostToStorage(userUID: String, data: URL, completed: @escaping (String) -> ()){
        let metaData = StorageMetadata()
        metaData.contentType = "video/mp4"
        let unique_filename = NSUUID.init().uuidString
        
        firebaseRef_ProfilePosts.child(userUID).child(unique_filename).putFile(from: data, metadata: metaData, completion: { (metadata, error) in
            if error != nil {
                completed("error")
            }
            else{
                completed(metadata!.downloadURL()!.absoluteString)
            }
        })
    }
    
    func uploadVideoToStorage(category: String, data: URL, completed: @escaping (String) -> ()){
        let metaData = StorageMetadata()
        metaData.contentType = "video/mp4"
        let unique_filename = NSUUID.init().uuidString
        
        STORAGE_BASE.child(category).child(unique_filename).putFile(from: data, metadata: metaData, completion: { (metadata, error) in
            if error != nil {
                completed("error")
            }
            else{
                completed(metadata!.downloadURL()!.absoluteString)
            }
        })
    }

    
    //MARK:----------------------------------------------------------------------------------
    //MARK: Get Data From Firebase Database
    //MARK:----------------------------------------------------------------------------------
    //MARK: Schedule
    func getDayClasses(day: String, completed: @escaping ([ScheduleData]) -> ()){
        var daySchedule = [ScheduleData]()
        firebaseRef_Schedule.child(day).observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children{
                let classSnapshot = child as! DataSnapshot
                let classTitle = classSnapshot.key
                let classTime = classSnapshot.value as! String
                let classData = ScheduleData(day: day, title: classTitle, time: classTime)
                daySchedule.append(classData)
            }
            daySchedule.sort{
                $0.time < $1.time
            }
            completed(daySchedule)
        })
    }
    
    func getUsersDayClasses(userUID: String, day: String, completed: @escaping ([ScheduleData]) -> ()){
        var daySchedule = [ScheduleData]()
        firebaseRef_Users.child(userUID).child("Schedule").child(day).observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children{
                let classSnapshot = child as! DataSnapshot
                let classTitle = classSnapshot.key
                let classTime = classSnapshot.value as! String
                let classData = ScheduleData(day: day, title: classTitle, time: classTime)
                daySchedule.append(classData)
            }
            daySchedule.sort{
                $0.time < $1.time
            }
            completed(daySchedule)
        })
    }
    
    
    //MARK: Videos
    func getHyperVideos(level: String, category: String, completed: @escaping ([VideoData]) -> ()){
        firebaseRef_Hyper.child(level).child(category).observeSingleEvent(of: .value, with: { (snapshot) in
            var videos = [VideoData]()
            for child in snapshot.children{
                let video = child as! DataSnapshot
                let title = video.key
                let url = video.value as! String
                let videoData = VideoData(videoTitle: title, videoURL: url)
                videos.append(videoData)
            }
            completed(videos)
        })
    }
    
    func getCurriculumVideos(beltColor: String, category: String, completed: @escaping ([VideoData]) -> ()){
        firebaseRef_Curriculum.child(beltColor).child(category).observeSingleEvent(of: .value, with: { (snapshot) in
            var videos = [VideoData]()
            for child in snapshot.children{
                let video = child as! DataSnapshot
                let title = video.key
                let url = video.value as! String
                let videoData = VideoData(videoTitle: title, videoURL: url)
                videos.append(videoData)
            }
            completed(videos)
        })
    }
    
    func getWorkoutVideos(completed: @escaping ([VideoData]) -> ()){
        firebaseRef_Workout.observeSingleEvent(of: .value, with: { (snapshot) in
            var videos = [VideoData]()
            for child in snapshot.children{
                let video = child as! DataSnapshot
                let title = video.key
                let url = video.value as! String
                let videoData = VideoData(videoTitle: title, videoURL: url)
                videos.append(videoData)
            }
            completed(videos)
        })
    }
    
    //MARK: Newsfeed
    func getNewsfeedPosts(userUID: String, completed: @escaping ([NewsfeedData])  -> ()){
        firebaseRef_Newsfeed.observeSingleEvent(of: .value, with: { (snapshot) in
            var posts = [NewsfeedData]()
            for child in snapshot.children{
                let post = child as! DataSnapshot
                let postReference = post.key
                let postText = post.childSnapshot(forPath: "postText").value as! String
                let postAuthor = post.childSnapshot(forPath: "postAuthor").value as! String
                let postAuthorUID = post.childSnapshot(forPath: "postAuthorUID").value as! String
                let timestamp = post.childSnapshot(forPath: "timestamp").value as! Double
                let postAuthorPicURL = post.childSnapshot(forPath: "postAuthorPicURL").value as! String
                let isMyPost = postAuthorUID == userUID ? true : false
                let postData = NewsfeedData(postReference: postReference, postText: postText, postAuthor: postAuthor, postAuthorUID: postAuthorUID, postAuthorPicURL: postAuthorPicURL, isMyPost: isMyPost, timestamp: timestamp)
                posts.append(postData)
            }
            posts.sort{
                $0.timestamp > $1.timestamp
            }
            completed(posts)
        })
        
    }
    //MARK: Store
    func getStorePosts(completed: @escaping ([StoreData])  -> ()){
        firebaseRef_Store.observeSingleEvent(of: .value, with: { (snapshot) in
            var store = [StoreData]()
            for child in snapshot.children{
                let item = child as! DataSnapshot
                let title = item.childSnapshot(forPath: "title").value as! String
                let imageURL = item.childSnapshot(forPath: "imageURL").value as! String
                let price = item.childSnapshot(forPath: "price").value as! String
                let uid = item.key
                let storeItem = StoreData(title: title, imageURL: imageURL, price: price, uid: uid)
                store.append(storeItem)
            }
            completed(store)
        })
    }

    //MARK: Profile
    func getProfilePhotos(userUID: String, completed: @escaping ([ProfilePhotoData])  -> ()){
        var photos = [ProfilePhotoData]()
        firebaseRef_Profile.child(userUID).child("Posts").observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children{
                let post = child as! DataSnapshot
                let mediaReference = post.key
                let mediaURL = post.childSnapshot(forPath: "photoURL").value as! String
                let type = post.childSnapshot(forPath: "type").value as! String
                let timestamp = post.childSnapshot(forPath: "timestamp").value as! Double
                let caption = post.childSnapshot(forPath: "caption").value as! String
                let photoData = ProfilePhotoData(mediaReference: mediaReference, mediaURL: mediaURL, type: type, caption: caption, timestamp: timestamp)
                photos.append(photoData)
            }
            photos.sort{
                $0.timestamp > $1.timestamp
            }
            completed(photos)
        })
    }
    
    func getProfileNotifications(userUID: String, completed: @escaping ([NotificationData])  -> ()){
        var notifications = [NotificationData]()
        firebaseRef_Profile.child(userUID).child("Notifications").observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children{
                let notification = child as! DataSnapshot
                let senderName = notification.childSnapshot(forPath: "senderName").value as! String
                let text = notification.childSnapshot(forPath: "text").value as! String
                let timestamp = notification.childSnapshot(forPath: "timestamp").value as! Double
                let notificationData = NotificationData(notificationText: text, senderName: senderName, timestamp: timestamp)
                notifications.append(notificationData)
            }
            notifications.sort{
                $0.timestamp > $1.timestamp
            }
            completed(notifications)
        
        })
    }
    
    //MARK: Users
    func getAllUsers(completed: @escaping ([UserData])  -> ()){
        var users = [UserData]()
        firebaseRef_Users.observeSingleEvent(of: .value, with: { (snapshot) in
            for child in snapshot.children{
                let userSnapshot = child as! DataSnapshot
                let firstName = userSnapshot.childSnapshot(forPath: "firstName").value as! String
                let lastName = userSnapshot.childSnapshot(forPath: "lastName").value as! String
                let username = userSnapshot.childSnapshot(forPath: "username").value as! String
                let uid = userSnapshot.childSnapshot(forPath: "uid").value as! String
                let email = userSnapshot.childSnapshot(forPath: "email").value as! String
                let beltColor = userSnapshot.childSnapshot(forPath: "beltColor").value as! String
                let profilePicURL = userSnapshot.childSnapshot(forPath: "profilePic").value as! String
                let user = UserData(firstName: firstName, lastName: lastName, username: username, uid: uid, email: email, beltColor: beltColor, profilePicURL: profilePicURL)
                users.append(user)
            }
            users.sort{
                $0.lastName > $1.lastName
            }
            completed(users)
        })
    }
    
    func getUserData(userUID: String, completed: @escaping (UserData)  -> ()){
        firebaseRef_Users.child(userUID).observeSingleEvent(of: .value, with: { (snapshot) in
            let uid = snapshot.key
            let firstName = snapshot.childSnapshot(forPath: "firstName").value as! String
            let lastName = snapshot.childSnapshot(forPath: "lastName").value as! String
            let username = snapshot.childSnapshot(forPath: "username").value as! String
            let email = snapshot.childSnapshot(forPath: "email").value as! String
            let beltColor = snapshot.childSnapshot(forPath: "beltColor").value as! String
            let profilePicURL = snapshot.childSnapshot(forPath: "profilePic").value as! String
            let user = UserData(firstName: firstName, lastName: lastName, username: username, uid: uid, email: email, beltColor: beltColor, profilePicURL: profilePicURL)
            completed(user)
        })
    }

    //MARK: Authentication
    func getAdminCode(completed: @escaping (String)  -> ()){
        firebaseRef_AdministratorCode.observe(.value, with: { (snapshot) in
            let code = snapshot.value as! String
            completed(code)
        })
    }
    
    func getKickForceCode(completed: @escaping (String)  -> ()){
        firebaseRef_KickForceCode.observe(.value, with: { (snapshot) in
            let code = snapshot.value as! String
            completed(code)
        })
    }
    
    //MARK:----------------------------------------------------------------------------------
    //MARK: Delete from Firbease Database
    //MARK:----------------------------------------------------------------------------------
    
    func deleteHyperVideo(level: String, category: String, videoTitle: String, completed: @escaping ()  -> ()){
        firebaseRef_Hyper.child(level).child(category).child(videoTitle).removeValue()
        completed()
    }
    
    func deleteWorkoutVideo(videoTitle: String, completed: @escaping ()  -> ()){
        firebaseRef_Workout.child(videoTitle).removeValue()
        completed()
    }
    
    func deleteCurriculumVideo(level: String, category: String, videoTitle: String, completed: @escaping ()  -> ()){
        firebaseRef_Curriculum.child(level).child(category).child(videoTitle).removeValue()
        completed()
    }
    
    func deleteStoreItem(itemUID: String){
        firebaseRef_Store.child(itemUID).removeValue()
    }
    
    func deleteProfilePhoto(userUID: String, photoRef: String){
        firebaseRef_Profile.child(userUID).child("Posts").child(photoRef).removeValue()
    }
    
    func deleteUsersClass(userUID: String, day: String, title: String){
        firebaseRef_Users.child(userUID).child("Schedule").child(day).child(title).removeValue()
    }
    
    func deleteClass(day: String, title: String){
        firebaseRef_Schedule.child(day).child(title).removeValue()
    }
    
    func deleteNewsfeedPost(postRef: String){
        firebaseRef_Newsfeed.child(postRef).removeValue()
    }
    
    func deleteOldestPost(){
        firebaseRef_Newsfeed.observeSingleEvent(of: .value, with: { (snapshot) in
            let numPosts = snapshot.childrenCount
            var oldestTime = Date().timeIntervalSince1970
            var oldestPost: DataSnapshot!
            if numPosts > 500{
                for post in snapshot.children{
                    let newsfeedPost = post as! DataSnapshot
                    let timestamp = newsfeedPost.childSnapshot(forPath: "timestamp").value as! Double
                    if timestamp < oldestTime{
                        oldestTime = timestamp
                        oldestPost = newsfeedPost
                    }
                }
                self.firebaseRef_Newsfeed.child(oldestPost.key).removeValue()
            }
        })
    }
}

