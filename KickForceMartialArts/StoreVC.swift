//
//  StoreVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import MessageUI

class StoreVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, MFMailComposeViewControllerDelegate{
    
    //MARK: Properties
    var storeItems = [StoreData]()
    static var imageCache: NSCache<NSString, UIImage> = NSCache()

    
    //MARK: IBOutlet
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()
        
        collectionView.delegate = self
        collectionView.dataSource = self

        DataService.singleton.getStorePosts(completed: { (store) in
            self.storeItems = store
            self.collectionView.reloadData()
            self.activitySpinner.stopAnimating()
        })
        
    }
    
    //MARK: UICollectionViewDelegate, UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return storeItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StoreCell", for: indexPath) as! StoreCell
        
        if let cachedImage = StoreVC.imageCache.object(forKey: storeItems[indexPath.row].imageURL as NSString){
            cell.itemImage.image = cachedImage
            cell.configureCell(storeItem: storeItems[indexPath.row], needImage: false)
        }else{
            cell.configureCell(storeItem: storeItems[indexPath.row], needImage: true)
        }
        cell.tapAction = { (cell) in
            self.requestPurchase(title: self.storeItems[indexPath.row].title, price: self.storeItems[indexPath.row].price)
        }
        
        return cell
    }
    //MARK: UICollectionViewDelegateFlowLayout
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let side = (UIScreen.main.bounds.width - 20) / 2
        return CGSize(width: side, height: side + 90)
    }
    
    //MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    //Composes email to kickforce requesting a purchase of the selected item
    func requestPurchase(title: String, price: String){
        //email
        let mailComposeViewController = MFMailComposeViewController()
        mailComposeViewController.mailComposeDelegate = self
        mailComposeViewController.setToRecipients(["kickforcemartialarts@gmail.com"])
        mailComposeViewController.setSubject("Request for KickForce merchandise purchase")
        
        mailComposeViewController.setMessageBody("KickForce Martial Arts, \n\n I would like to purchase \(title) for \(price)\nDetails: \n\n\n Thank you,\n", isHTML: false)
        
        if MFMailComposeViewController.canSendMail() {
            self.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            GlobalActions.singleton.displayAlert(sender: self, title: "Error sending email", message: "Your device could not send email at this time. Please check your email configuration and try again")
        }
    }
    
    
}
