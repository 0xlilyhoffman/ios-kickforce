//
//  NotificationsVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class NotificationsVC: UIViewController, UITableViewDataSource, UITableViewDelegate{
    
    //MARK: Properties
    var firebaseUser: User!
    var notifications = [NotificationData]()
    
    //MARK: IBOutlets
    @IBOutlet var tableView: UITableView!
    @IBOutlet var activitySpinner: UIActivityIndicatorView!
    
    //MARK: Init
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firebaseUser = Auth.auth().currentUser
        
        activitySpinner.hidesWhenStopped = true
        activitySpinner.startAnimating()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        
        DataService.singleton.getProfileNotifications(userUID: firebaseUser.uid, completed: { (notifications) in
            self.notifications = notifications
            self.tableView.reloadData()
            self.activitySpinner.stopAnimating()
        })
    }
    
    //MARK: UITableViewDataSource, UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
        cell.configureCell(notification: notifications[indexPath.row])
        return cell
    }
}
