//
//  MenuVC.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit


class MenuVC: UITableViewController{
    
    
    //For Administration section of app - require special authentication popup screen
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 8{
            let blurEffectView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.dark))
            blurEffectView.frame = self.view.frame
            self.view.addSubview(blurEffectView)
            
            performSegue(withIdentifier: "AdminLogin", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AdminLogin"{
            let destVC = segue.destination as! AdministrationVC
            destVC.presentingVC = self
        }
    }
}
