//
//  RoundedButton.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/6/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class RoundedButton: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        
        layer.backgroundColor = UIColor.lightGray.cgColor
        layer.cornerRadius = 15.0
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 1.0, height: 1.0)
        layer.shadowRadius = 10.0
        layer.shadowOpacity = 0.8
        layer.masksToBounds = false
    }
    
}
