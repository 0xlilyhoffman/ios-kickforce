//
//  StoreData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class StoreData{
    private var _title: String!
    private var _imageURL: String!
    private var _price: String!
    private var _image: UIImage!
    private var _uid: String!
    
    var title: String{
        return _title
    }
    var imageURL: String{
        return _imageURL
    }
    var price: String{
        return _price
    }
    var image: UIImage{
        return _image
    }
    
    var uid: String{
        return _uid
    }
    
    init(title: String, imageURL: String, price: String, uid: String){
        self._title = title
        self._imageURL = imageURL
        self._price = price
        self._uid = uid
        self._image = GlobalActions.singleton.getImage(from: imageURL)
    }
    
    
}
