//
//  NewsfeedData.swift
//  KickForceMartialArts
//
//  Created by Lily Hoffman on 8/5/17.
//  Copyright © 2017 Lily Hoffman. All rights reserved.
//

import Foundation
import UIKit

class NewsfeedData{
    private var _postReference: String
    private var _postText: String
    private var _postAuthor: String!
    private var _postAuthorUID: String!
    private var _postAuthorPicURL: String!
    private var _isMyPost: Bool!
    private var _timestamp: Double!
    private var _dateString: String!
    
    var postReference: String{
        return _postReference
    }
    var postText: String{
        return _postText
    }
    var postAuthor: String{
        return _postAuthor
    }
    var postAuthorUID: String{
        return _postAuthorUID
    }
    var postAuthorPicURL: String{
        return _postAuthorPicURL
    }
    var postAuthorPic: UIImage{
        return GlobalActions.singleton.getImage(from: self._postAuthorPicURL)
    }
    var isMyPost: Bool{
        return _isMyPost
    }
    var timestamp: Double{
        return _timestamp
    }
    var dateString: String{
        return _dateString
    }
    
    init(postReference: String, postText: String, postAuthor: String, postAuthorUID: String, postAuthorPicURL: String, isMyPost: Bool, timestamp: Double){
        self._postReference = postReference
        self._postText = postText
        self._postAuthor = postAuthor
        self._postAuthorUID = postAuthorUID
        self._postAuthorPicURL = postAuthorPicURL
        self._isMyPost = isMyPost
        self._timestamp = timestamp
        self._dateString = GlobalActions.singleton.getDateString(from: timestamp)
    }

}

